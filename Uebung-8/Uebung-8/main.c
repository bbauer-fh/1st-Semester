//
//  main.c
//  Uebung 8
//
//  Created by Bernhard Bauer on 09.01.18.
//  Copyright © 2018 bbmk IT solutions gmbh. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "image.h"

int main (int argc, const char **argv) 
{
    printf("Started..\n");
    
	struct header bmpHeader;
	struct info bmpInfo;
	struct pixel *data;
	struct pixel *mirrorData, *invertData, *saturationData;
	
	data = readBitmap(argv[1], &bmpHeader, &bmpInfo);
    mirrorData = mirror(&bmpInfo, data);
    writeBitmap(argv[2], &bmpHeader, &bmpInfo, mirrorData);
//    invertData = invert(&bmpInfo, data);
//    writeBitmap(argv[2], &bmpHeader, &bmpInfo, invertData);
//    saturationData = saturation(&bmpInfo, data, 0.5, 0.5, 0.5);
//    writeBitmap(argv[2], &bmpHeader, &bmpInfo, saturationData);
	
	/*
	 * GEBEN SIE NICHT MEHR BENÖTIGTE SPEICHERBEREICHE SPÄTESTENS AN DIESER STELLE
	 * WIEDER FREI.
	 */
    return 0;
}

