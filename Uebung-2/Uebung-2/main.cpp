//
//  main.cpp
//  Uebung-2
//
//  Created by Bernhard Bauer on 16.11.17.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include <iostream>

using namespace std;

void uebung_beispiel1() {
    cout << "=> Übung Beispiel 1: Mittelwert von 3 Zahlen berechnen" << endl;
    double sum = 0, input;
    
    cout << "Geben Sie 3 Zahlen ein, aus welchen der Mittelwert berechnet werden soll" << endl;
    for (int i=0; i<3; i++) {
        cout << i+1 << ". Zahl: ";
        cin >> input;
        sum += input;
    }
    
    cout << "Mittelwert: " << sum / 3 << endl;
}

void uebung_beispiel2() {
    cout << "=> Übung Beispiel 2: Füllmenge eines Schwimmbeckens berechnen" << endl;
    double kubikmeter = 0, input;
    
    cout << "Geben Sie 3 Zahlen ein, aus welchen der Mittelwert berechnet werden soll" << endl;
    for (int i=0; i<3; i++) {
        switch (i) {
            case 0:
                cout << "Breite eingeben: ";
                break;
            case 1:
                cout << "Länge eingeben: ";
                break;
            case 2:
                cout << "Höhe eingeben: ";
                break;
        }
        
        cin >> input;
        if (kubikmeter != 0) {
            kubikmeter = kubikmeter * input;
        } else {
            kubikmeter = input;
        }
    }
    
    cout << "Füllmenge: " << kubikmeter * 1000 << endl;
}

void freiwillig_beispiel2(int len) {
    cout << "=> Frewillig Beispiel 2: Pyramide aufbauen" << endl;
    for (int i=0; i<len; i++) {
        int start = i%2;
        
        for (int j=start; j<(i+start); j++) {
            cout << j%2;
        }
        cout << endl;
    }
}

void freiwillig_beispiel3() {
    cout << "=> Frewillig Beispiel 3: Alle Primzahlen zwischen start und ende ausgeben" << endl;
    int x, y;

    cout << "Bitte geben Sie einen Startwert ein: ";
    cin >> x;
    cout << "Bitte geben Sie einen Endwert ein: ";
    cin >> y;

    if (x > 1) {
        for (int i=x; i<y; i++) {
            bool is_prime = true;
            
            for (int j=2; j<i; j++) {
                if ((i % j) == 0) {
                    is_prime = false;
                    break;
                }
            }
            
            if (is_prime) {
                cout << i << " ist eine Primzahl" << endl;
            }
        }
    } else {
        cout << "Der Startwert muss größer als 0 sein!" << endl;
    }
}

int main(int argc, const char * argv[]) {
    
    uebung_beispiel1();
    uebung_beispiel2();
    freiwillig_beispiel2(10);
    freiwillig_beispiel3();
    
    return 0;
}
