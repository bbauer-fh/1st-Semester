#include <stdio.h>

int main()
{

	int row, star, space;
	
	for (row = 1; row <= 6; row++)
	{
		for (space = 6 - row; space >= 1; space--)
		{
			printf(" ");
		}
		for (star =1 ; star <= (row*2)-1; star++)
		{
			printf("*");
		}
		printf("\n");
	}
	printf("\n");
	return 0;
}