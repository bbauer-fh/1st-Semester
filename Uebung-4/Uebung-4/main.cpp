//
//  main.cpp
//  Uebung-4
//
//  Created by Bernhard Bauer on 07.12.17.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include <iostream>
#include <math.h>

double f(double x) {
    return sin(x);
}

double my_bisection(double x0, double x1, double epsilon) {
    if ((f(x0) * f(x1)) < 0) {
        (x1-x0) / 2
        
        my_bisection(x0, x1, epsilon);
    }
    
    return 0;
}

int main(int argc, const char * argv[]) {
    std::cout << f(5) << std::endl;
    std::cout << my_bisection(0, 1, 0.1) << std::endl;
    
    return 0;
}
